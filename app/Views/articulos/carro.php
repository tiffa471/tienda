<?= $this->extend('comun/layout') ?>
<?= $this->section('contenido') ?>
<?php $session = \Config\Services::session(); ?>
<?php if ($session->has('carro')):?>
    <?php $carro = $session->get('carro');?>
    <div class="row">
        <table class="table table-striped">
            <?php foreach ($carro as $articulo): ?>
                <tr>
                    <td><img src="<?php echo base_url("assets/images/camisetas/".str_replace(" ","_",$articulo->descripcion).".jpg");?>" width="60px;"></td>
                    <td><?= $articulo->descripcion?></td>
                    <td><?= $articulo->precio?></td>
                    <td><?= $articulo->cantidad?></td>
                </tr>
            <?php endforeach; ?>
        </table>
        <a href="<?= site_url('tienda/borraCarro')?>" class="btn btn-danger">Vacía carro</a>
        <a href="<?= site_url('/')?>" class="btn btn-secondary">Seguir Comprando</a>
    </div>
    <?php else : ?>
    <h3>No hay artículos</h3>
    <p>El carro está vacio</p>
    <a href="<?= site_url('tienda/index')?>" class="btn btn-primary">Volver a la página principal</a>
<?php endif ?>
<?= $this->endSection() ?>
<?= $this->section('titulo') ?>
<?= $titulo ?>
<?= $this->endSection() ?>