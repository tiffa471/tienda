<?php namespace App\Controllers;

use App\Models\Datos;
use App\Models\DisenadoresModel;
use Config\Services;

class Tienda extends BaseController {

    protected $session;
    protected $datos;

    public function insertar()
    {
        echo "Insertar un nuevo Artículo ";
        $articulo = [
            'descripcion' => 'retro siluet',
            'stock' => 5,
            'precio' => 33.9
        ];
        $this->datos->insert($articulo);
    }

    public function save()
    {
        $articulo = [
            'id' => 42,
            'stock' => 2,
            'precio' => 23.9
        ];
        $this->datos->save($articulo);
    }

    public function borrar($id = 0)
    {
        if ($id !== 0)
        {
            $this->datos->delete($id);
            return redirect()->to(site_url('tienda'));
        }
        else
        {
            echo "Debes especificar un artículo";
        }
    }

    public function index()
    {
        $data = [
            'titulo' => "Listado de Artículos Paginados",
            'articulos' => $this->datos->select('articulos.id,descripcion, precio, stock,apodo, nombre')->join('disenadores','disenadores.id = articulos.disenador','left')->paginate(8),
            'pager' => $this->datos->pager
        ];
        //echo '<pre>';
        $this->datos->pager->setPath('tienda/tienda/index');
        //print_r($data['articulos']);
        //echo '</pre>';
        echo view('articulos/mostrar', $data);
    }

    public function form_alta()
    {
        $data ['titulo'] = "Nuevo artículo";
        helper('form');
        echo view('articulos/form_alta', $data);
    }

    public function nuevo()
    {
        helper('form');
        $data ['titulo'] = "Nuevo artículo";
        //regla para validar la imagen.
        $image_validate = 
        [
            'imagen' => 'uploaded[imagen]|ext_in[imagen,jpg]|max_size[imagen,100]|max_dims[imagen,420,520]',
            'descripcion' => 'is_unique[articulos.descripcion]',
        ];
        if ($this->request->getVar('boton') != NULL && $this->validate(array_merge($this->datos->getValidationRules(),$image_validate)))
        {
            $articulo = $this->request->getPost();
            $this->datos->insert($articulo);
            //subimos y le asignamos un nombre al fichero
            try 
            {
                $path = $this->request->getFile('imagen')->store('camisetas/',str_replace(" ", "_", $articulo['descripcion'].".jpg"));
                $this->session->setFlashdata('mensaje_exito',"Registro insertado: {$articulo['descripcion']} ");
            } 
            catch (Exception $e) 
            {
                $this->session->setFlashdata('mensaje_exito',"Error al subir la imagen {$articulo['descripcion']}: ".$e->getMessage());
            }
            return redirect()->to(site_url('tienda/nuevo'));
        } 
        else 
        {
            $disenadoresModel = new DisenadoresModel();
            $data['options'] = $disenadoresModel->getDisenadores();
            echo view('articulos/form_alta', $data);
        }

    }
    
    public function edita($id)
    {
        helper('form');
        //recuperamos el artículo
        $data['articulo'] = $this->datos->find($id);
        //Quiero poner que en el titulo salga el nombre de la camiseta en vez de la id
        //$user = $userModel->findColumn($column_name);
        //$data['nombre'] = $this->datos->findColumn($descripcion);
        $data ['titulo'] = "Edita un artículo - {$data['articulo']->id} -";
        //regla para validar la imagen.
        $image_validate = 
        [
            'imagen' => 'uploaded[imagen]|ext_in[imagen,jpg]|max_size[imagen,100]|max_dims[imagen,420,520]',
            'descripcion' => 'is_unique[articulos.descripcion]',
        ];
        $disenadoresModel = new DisenadoresModel();
        $data['options'] = $disenadoresModel->getDisenadores();
        //Quiero que el valor por defecto que salga de cada diseñador sea el que toca y no el primero que sale
        //$data['options'] = $disenadoresModel->getDisenadoresEdita($data);
        if ($this->request->getVar('boton') != NULL && $this->validate(array_merge($this->datos->getValidationRules(),$image_validate)))
        {
            $articulo = $this->request->getPost();
            $this->datos->update($id, $articulo);
            $data['mensaje'] = 'Cambio realizado con éxito';
            try //Fatla poner que se vea la imagen actual
            {
                $path = $this->request->getFile('imagen')->store('camisetas/',str_replace(" ", "_", $articulo['descripcion'].".jpg"));
                $this->session->setFlashdata('mensaje_exito',"Registro insertado: {$articulo['descripcion']} ");
            } 
            catch (Exception $e) 
            {
                $this->session->setFlashdata('mensaje_exito',"Error al subir la imagen {$articulo['descripcion']}: ".$e->getMessage());
            }
            return redirect()->to(site_url('tienda/edita'));
        }
        echo view('articulos/form_edita', $data);
    }
        
    public function setLanguage($locale)
    {
        $this->session->set('locale', $locale);
        return redirect()->back();
    }   

    public function initController(\CodeIgniter\HTTP\RequestInterface $request,\CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
        //------------------------------------------------------------
        // Preload any models, libraries, etc, here.
        //------------------------------------------------------------
        $this->datos = new Datos();
        $this->session = Services::session();
    }

    public function comprar ($id)
    {
        //Comprobar si el artículo está en el carro
        if ($this->session->has('carro')) 
        {
            //existe el carro
            $carro = $this->session->get('carro');
            if (!isset($carro[$id]))
            {
                //no existe el elmento
                $articulo = $this->datos->find($id);
                $articulo->cantidad = 1;
                $carro[$id]=$articulo; //añadimos un elemento al array
            } 
            else 
            {
                //sólo incrementar la cantidad comprada
                $carro[$id]->cantidad++;
            }
        } 
        else 
        {
            //crear el carro
            $articulo = $this->datos->find($id);
            $carro[$id] = $articulo;
            $articulo->cantidad = 1;
        }
        //var_dump($this->session->carro);
        //y en todos los casos guardar la variable de sesion
        $this->session->set('carro',$carro);
        return redirect()->to(site_url('/'));
        }

        public function carro()
        {
            //definimos como queremos paginar
            $data = ['titulo' => "Carro de la compra",];
            //Ahora los datos no hemos de buscarlos en la base de datos,
            // están en la variable de sesión.
            echo view('articulos/carro', $data);
        }
        
        public function borraCarro()
        {
            $this->session->remove('carro');
            return redirect()->to(site_url('tienda/carro'));
        }
            
        

}